﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector
{
    class Vector
    {
        static int[] Entero = new int[20];

        public void LeeDatos()
        {
            Console.WriteLine("INGRESA 20 NUMEROS: ");

            for (int i = 0; i < Entero.Length; i++)
            {
                Entero[i] = int.Parse(Console.ReadLine());

            }
            MostrarDatos(Entero);
        }

        public void MostrarDatos(int[] Entero)
        {
            Console.WriteLine("Mostrar Numeros Ingresados");

            for (int i = 0; i < Entero.Length; i++)
            {
                Console.Write(Entero[i] + ", ");
            }
        }


    }
}
