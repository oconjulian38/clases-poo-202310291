﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaDestructores
{
    class Prueba
    {
        public int x;
        
        public Prueba(int x)
        {
            
            this.x = 5;
        }

        

        public int prueba(int x)
        {
            System.Console.Write("Creado objeto con x={0}",x);
            Console.ReadLine();
            return this.x;
        }

        ~Prueba()
        {
            this.x = 0;
        }

    }
}
