﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaHerencia
{
    class Herencia
    {
        //Se declaran los atributos junto con sus modificadores de acceso
        public int atributo1;
        private int atributo2;
        protected int atributo3;


        //Se crea un metodo public en la clase Herencia
        public void metodo1()
        {
            //Declaramos lo que queremos que nos haga el metodo
            Console.WriteLine("Este es el metodo 1 de Herencia");
        }

        //Se crea un metodo Private en la clase Herencia
        private void metodo2()
        {
            //Declaramos lo que queremos que nos haga el metodo
            Console.WriteLine("Este metodo no es accesible desde afuera de la clase Herencia");
        }

        //Se crea un metodo Protected en la clase Herencia
        protected void metodo3()
        {
            //Declaramos lo que queremos que nos haga el metodo
            Console.WriteLine("Este es el metodo 3 de Herencia protected");
        }

        //Hacemos un metodo en la clase Herencia para llamar a otro metodo private de la misma clase
        public void AccesoMetodo2()
        {
            //Lamamos a el metodo private que queremos llamar
            metodo2();
        }

    }


    //Creamos otra clase que haga referencia a la clase a la que queremos acceder a sus metodos 
    class Hijo: Herencia
    {
        // Se crea un metodo 
        public void AccesoMetodo3()
        {
            //Llamamos a un metodo protected de la clase a la que hacemos referencia 
            metodo3();
        }
    }
}
