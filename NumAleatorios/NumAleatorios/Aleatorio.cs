﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumAleatorios
{
    class Aleatorio
    {
         Random rnd = new Random();
         public int num;
         public int contador = 1;

         ~Aleatorio()
         {
             this.num = 0;
         }

         public void aleatorio()
         {
             while (contador <= 10)
             {
                 contador++; 
                 num = rnd.Next(1, 251);
                 Console.WriteLine("El numero es " + num);
             }

         }

         public int ImprimirAleatorio()
         {
             return this.num;
         }
        
        
    }
}
