﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1U4Try_Catch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int n1, n2, total;
       
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                n1 = int.Parse(txt1.Text);
                n2 = int.Parse(txt2.Text);

                total = n1 * n2;
               
            }
            catch (Exception error)
            {
                MessageBox.Show("A ocurrido un error: " + error.Message);
            }
            finally
            {
                
                MessageBox.Show("El total de la multiplicacion es: " + total);
            }
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            try
            {
                n1 = int.Parse(txt1.Text);
                n2 = int.Parse(txt2.Text);

                total = (n1 / n2);
               
            }
            catch (DivideByZeroException error)
            {
                MessageBox.Show("A ocurrido un error: " + error.Message);
            }
            finally
            {
                MessageBox.Show("El total de la division es: " + total);
            }
        }  

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
