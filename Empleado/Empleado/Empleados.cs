﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empleado
{
    public abstract class Empleados
    {
        
        public double SalarioSemanal;
        public double Sueldo;
        public double SueldoBase;
        public double SueldoHoras;
        public double VentasBrutas;

        public abstract void CalculaSalario(double SA, double SH, int HT);
    }
}