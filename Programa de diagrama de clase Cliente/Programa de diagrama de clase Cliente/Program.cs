﻿using System;

namespace Programa_de_diagrama_de_clase_Cliente
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente cli1 = new Cliente();

            cli1.setEliminaCliente();
            string eliminacli1 = cli1.getEliminaCliente();

            cli1.setInsertaCliente();
            string insertarcli2 = cli1.getInsertaCliente();

            cli1.setMostrarCliente();
            string mostrarcli3 = cli1.getMostrarCliente();
        }
    }
}
