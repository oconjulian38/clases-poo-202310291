﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsExceptions05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnAgendar_Click(object sender, EventArgs e)
        {
            try
            {
                int Hora; 
                int Minuto;
                string Descripcion; 
                String Dia;

                Dia = txtDia.Text;
                Hora = int.Parse(txtHora.Text);
                Minuto = int.Parse(txtMinuto.Text);
                Descripcion = textDescripcion.Text;

                try
                {
                    Cita obj = new Cita(Dia, Hora, Minuto, Descripcion);
                }
                catch (ArgumentException EX)
                {
                    MessageBox.Show(EX.Message);
                }

                MessageBox.Show("Su cita sera" + " el dia " + Dia + " " + " a las: " + Hora + ":" + Minuto + "    " + "Descripcion de la cita :   " + Descripcion);
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnBorrar_Click(object sender, EventArgs e)
        {
            textDescripcion.Clear();
            txtHora.Clear();
            txtMinuto.Clear();
            txtDia.Clear();
        }
    }
}
