﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica1Throw
{
    class Clase
    {
        public int CalculaDivision(int numerador,int denominador)
        {
          if (denominador == 0)
          throw new Exception("El denominador NO debe ser cero");
          else
              return(numerador / denominador);
        }
    }
}
