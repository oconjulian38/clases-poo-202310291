﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica1Throw
{
    class Program
    {
        static void Main(string[] args)
        {
            int a , b,c=0;
            Console.WriteLine("Leer a y b");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            try
            {
              Clase e = new Clase();
              c=e.CalculaDivision(a,b);
            }
            catch (Exception x)
            {
              Console.WriteLine(x.Message);
            }
            finally
            {
              Console.WriteLine(a + "/" + b + "=" + c);
            }
              Console.ReadKey();
           }
    }
}
