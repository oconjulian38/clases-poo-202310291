﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica6SobrecargaVectores
{
    class Vector
    {
        
        public void Mostrar(params int[] a)
        {

            Console.WriteLine("INGRESA 20 NUMEROS: ");

            for (int i = 1; i < a.Length; i++)
            {
                Console.WriteLine("Numero " + i + ": ");
                a[i] = int.Parse(Console.ReadLine());

            }

            Console.WriteLine("-----Numeros----");
           Array.Sort(a);
           Array.Reverse(a);
            
            foreach (int i in a)
            {
                Console.WriteLine(+i);
            }
           
        }


        public void Mostrar(params long[] b)
        {

            Console.WriteLine("INGRESA 20 NUMEROS: ");

            for (int i = 1; i < b.Length; i++)
            {
                Console.WriteLine("Numero " + i + ": ");
                b[i] = long.Parse(Console.ReadLine());

            }

            Console.WriteLine("-----Numeros----");
            Array.Sort(b);
            Array.Reverse(b);

            foreach (int i in b)
            {
                Console.WriteLine(+i);
            }

        }






    }
}
