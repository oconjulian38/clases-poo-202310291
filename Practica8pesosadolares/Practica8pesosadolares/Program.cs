﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica8pesosadolares
{
    class Program
    {
        static void Main(string[] args)
        {
            double pesos = 800;
            double dolar = 19.80;
            double resultado;

            resultado = pesos / dolar;
            Console.WriteLine("Los pesos convertidos a dolares son: " + resultado);

            Console.ReadKey();
        }
    }
}
