﻿using System;

namespace Area_y_Perimetro_de_Rueda_y_Moneda
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia rueda = new Circunferencia();
            rueda.setArearueda();
            double arearueda = rueda.getArearueda();

            rueda.setPerimetrorueda();
            double perirueda = rueda.getPerimetrorueda();

            Circunferencia moneda = new Circunferencia();
            moneda.setAreamoneda();
            double areamoneda = moneda.getAreamone();

            moneda.setPerimetromone();
            double perimoneda = moneda.getPerimetromone();

            Console.WriteLine("Area y Perometro de la Rueda");
            Console.WriteLine("El area de la rueda es: " + arearueda);
            Console.WriteLine("El perimetro de la rueda es: " + perirueda);

            Console.WriteLine("Area y perimetro de la Moneda");
            Console.WriteLine("El area de la moneda es: " + areamoneda);
            Console.WriteLine("El perimetro de la moneda es: " + perimoneda);
        }
    }
}
