﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Area_y_Perimetro_de_Rueda_y_Moneda
{
    class Circunferencia
    {
        public int radio = 0;
        public double pi = 3.1416;
        public double arearueda = 0;
        public double perimetrorue = 0;
        public double areamone = 0;
        public double perimetromone = 0;
        public void setArearueda()
        {
            radio = 10;
            arearueda = pi * Math.Pow(radio, 2);
        }

        public double getArearueda()
        {
            return this.arearueda;
        }

        public void setPerimetrorueda()
        {
            radio = 10;
            perimetrorue = pi * 2 * radio;
        }

        public double getPerimetrorueda()
        {
            return this.perimetrorue;
        }
        
        public void setAreamoneda()
        {
            radio = 1;
            areamone = pi * Math.Pow(radio, 2);
        }

        public double getAreamone()
        {
            return this.areamone;
        }

        public void setPerimetromone()
        {
            radio = 1;
            perimetromone = pi * 2 * radio;
        }

        public double getPerimetromone()
        {
            return this.perimetromone;
        }

 
    }
}
