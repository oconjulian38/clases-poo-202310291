﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrizArreglo
{
    class Operacion
    {
        public int n;
        public int m;
        public int[,] Matriz;
       
        public void ImprimeMatriz(int n, int m, int[,] Matriz) 
        {
            for (int i=0; i<n; i++)
            {
                for (int j=0; j<m; j++)
                {
                    Console.Write("" + Matriz[i, j]);
                }
            }
        }

        public void LeerMatriz(int n, int m)
        {
            int[,] Matriz = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.WriteLine("Ingresa los numeros de la matriz");
                    Console.Write("[" + i + "," + j + "]" + " = ");
                    Matriz[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }   
    }
}
