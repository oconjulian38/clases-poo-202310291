﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloOperacion
{
    class Program
    {
        static void Main(string[] args)
        {

            Operacion sumaArreglo = new Operacion();
            Operacion parArreglo = new Operacion();

            int[] n = new int[10];


            for (int i = 0; i < n.Length; i++)
            {
                Console.WriteLine("Ingresa un numero");
                int num = Convert.ToInt32(Console.ReadLine());
                n[i] = num;
            }
            Console.WriteLine("La suma es: " + sumaArreglo.GetSuma(n));
            Console.WriteLine("Los pares son: " + parArreglo.GetPares());


            
            Console.ReadKey();
        }
    }
}
