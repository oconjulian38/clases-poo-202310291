﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloOperacion
{
    class Operacion
    {
        public int suma = 0;
        public int n;
        public int par = 0;

        public int GetSuma(int[] n)
        {
            foreach (int val in n)
            {
                suma = suma + val;
            }

            return this.suma;
        }

        public int GetPares()
        {
            par = Convert.ToInt32(n % 2 == 0);
            return this.par;
        }    
            
     }       
}

