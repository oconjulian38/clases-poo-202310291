﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2SobrecargadeMetodosGrande
{
    class Mayor
    {
        public void Ordenar(params int[] a)
        {
            Console.WriteLine("numeros ordenados");
            Array.Sort(a);
            Array.Reverse(a);
            foreach (int i in a)
            {
                Console.WriteLine(+i);
            }
        }

        public void Ordenar(params float[] b)
        {
            Console.WriteLine("numeros ordenados");
            Array.Sort(b);
            Array.Reverse(b);
            foreach (int i in b)
            {
                Console.WriteLine(+i);
            }
        }

        public void Ordenar(params double[] c)
        {
            Console.WriteLine("numeros ordenados");
            Array.Sort(c);
            Array.Reverse(c);
            foreach (int i in c)
            {
                Console.WriteLine(+i);
            }
        }

    }
}
