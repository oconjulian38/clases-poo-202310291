﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2SobrecargadeMetodosGrande
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[2];
            float[] b = new float[3];
            double[] c = new double[4];

            Mayor obj = new Mayor();

            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < b.Length; i++)
            {
               b[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = Convert.ToInt32(Console.ReadLine());
            }


            obj.Ordenar(a);
            obj.Ordenar(b);
            obj.Ordenar(c);

            Console.ReadLine();
        }
    }
}
