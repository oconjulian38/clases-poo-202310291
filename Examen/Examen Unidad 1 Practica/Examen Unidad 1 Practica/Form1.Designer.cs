﻿
namespace Examen_Unidad_1_Practica
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtClase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtClase
            // 
            this.BtClase.BackColor = System.Drawing.Color.Cyan;
            this.BtClase.Location = new System.Drawing.Point(314, 69);
            this.BtClase.Name = "BtClase";
            this.BtClase.Size = new System.Drawing.Size(162, 58);
            this.BtClase.TabIndex = 0;
            this.BtClase.Text = "Lavadora";
            this.BtClase.UseVisualStyleBackColor = false;
            this.BtClase.Click += new System.EventHandler(this.BtClase_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cyan;
            this.BackgroundImage = global::Examen_Unidad_1_Practica.Properties.Resources.lavadora;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(573, 283);
            this.Controls.Add(this.BtClase);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtClase;
    }
}

