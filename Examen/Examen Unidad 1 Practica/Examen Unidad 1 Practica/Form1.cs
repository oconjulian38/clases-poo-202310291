﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen_Unidad_1_Practica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtClase_Click(object sender, EventArgs e)
        {
            Lavadora LV = new Lavadora();

            LV.setCapacidad(14);
            int capacidadLV = LV.getCapacidad();

            LV.setColor("Blanca");
            string colorLV = LV.getColor();

            LV.setMarca("Mabe");
            string marcaLV = LV.getMarca();

            LV.setCiclos(3);
            int ciclosLV = LV.getCiclos();

            LV.setTipo("Automatica");
            string tipoLV = LV.getTipo();

            MessageBox.Show("La capacidad de la lavadora es de: " + capacidadLV + " kilos");
            MessageBox.Show("El color de la lavadora es: " + colorLV);
            MessageBox.Show("La marca de la lavadora es: " + marcaLV);
            MessageBox.Show("Los ciclos de la lavadora son: " + ciclosLV);
            MessageBox.Show("El tipo de la lavadora es: " + tipoLV);
        }
    
    }
}
