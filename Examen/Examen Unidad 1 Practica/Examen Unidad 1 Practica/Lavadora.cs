﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_Unidad_1_Practica
{
    class Lavadora
    {
        public int capacidad = 0;
        public string color = "";
        public string marca = "";
        public int ciclos = 0;
        public string tipo = "";

        public void setCapacidad(int capacidad)
        {
            this.capacidad = capacidad;
        }

        public int getCapacidad()
        {
            return this.capacidad;
        }

        public void setColor(string color)
        {
            this.color = color;
        }

        public string getColor()
        {
            return this.color;

        }

        public void setMarca(string marca)
        {
            this.marca = marca;
        }

        public string getMarca()
        {
            return this.marca;
        }

        public void setCiclos(int ciclos)
        {
            this.ciclos = ciclos;
        }

        public int getCiclos()
        {
            return this.ciclos;
        }

        public void setTipo(string tipo)
        {
            this.tipo = tipo;
        }

        public string getTipo()
        {
            return this.tipo;
        }
    }
}
