﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1PolimorfismoU4
{
    class Triangulo: Figuras
    {
        public double Total;

        public override void CalcularFiguras(double n1, double n2)
        {
            base.CalcularFiguras(n1, n2);
            Total = (n1 * n2) / 2;
        }
    }
}
