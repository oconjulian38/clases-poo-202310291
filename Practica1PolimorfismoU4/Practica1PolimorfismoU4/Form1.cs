﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1PolimorfismoU4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (RBr.Checked == true)
            {
                Rectangulos objr = new Rectangulos();
                double AnchoRec = double.Parse(txtA.Text);
                double LargoRec = double.Parse(txtL.Text);
                objr.CalcularFiguras(AnchoRec, LargoRec);
                MessageBox.Show("El area del Rectangulo es: " + objr.Total);
            }

            if (RBc.Checked == true)
            {
                Circulos objc = new Circulos();
                double RadioC = double.Parse(txtR.Text);
                objc.CalcularFiguras(RadioC, 0);
                MessageBox.Show("El area del circulo es: " + objc.Radio);
            }

            if (RBt.Checked == true)
            {
                Triangulo objt = new Triangulo();
                double Alturat = double.Parse(txtH.Text);
                double Anchot = double.Parse(txtA.Text);
                objt.CalcularFiguras(Alturat, Anchot);
                MessageBox.Show("El area del triangilo es: " + objt.Total);
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RBr_CheckedChanged(object sender, EventArgs e)
        {
            if (RBr.Checked == true)
            {
                txtR.Enabled = false;
                txtH.Enabled = false;
            }
            else
            {
                txtR.Enabled = true;
                txtH.Enabled = true;
            }
        }

        private void RBc_CheckedChanged(object sender, EventArgs e)
        {
            if (RBc.Checked == true)
            {
                txtH.Enabled = false;
                txtL.Enabled = false;
                txtA.Enabled = false;
            }
            else
            {
                txtH.Enabled = true;
                txtL.Enabled = true;
                txtA.Enabled = true;
            }
        }

        private void RBt_CheckedChanged(object sender, EventArgs e)
        {
            if (RBt.Checked == true)
            {
                txtR.Enabled = false;
                txtL.Enabled = false;
            }
            else
            {
                txtR.Enabled = true;
                txtL.Enabled = true;
            }
        }
    }
}
