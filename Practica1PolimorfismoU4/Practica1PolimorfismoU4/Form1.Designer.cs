﻿namespace Practica1PolimorfismoU4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RBt = new System.Windows.Forms.RadioButton();
            this.RBc = new System.Windows.Forms.RadioButton();
            this.RBr = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtH = new System.Windows.Forms.TextBox();
            this.txtR = new System.Windows.Forms.TextBox();
            this.txtA = new System.Windows.Forms.TextBox();
            this.txtL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RBt);
            this.groupBox1.Controls.Add(this.RBc);
            this.groupBox1.Controls.Add(this.RBr);
            this.groupBox1.Location = new System.Drawing.Point(12, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(159, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Figuras";
            // 
            // RBt
            // 
            this.RBt.AutoSize = true;
            this.RBt.Location = new System.Drawing.Point(41, 85);
            this.RBt.Name = "RBt";
            this.RBt.Size = new System.Drawing.Size(69, 17);
            this.RBt.TabIndex = 2;
            this.RBt.TabStop = true;
            this.RBt.Text = "Triangulo";
            this.RBt.UseVisualStyleBackColor = true;
            this.RBt.CheckedChanged += new System.EventHandler(this.RBt_CheckedChanged);
            // 
            // RBc
            // 
            this.RBc.AutoSize = true;
            this.RBc.Location = new System.Drawing.Point(41, 56);
            this.RBc.Name = "RBc";
            this.RBc.Size = new System.Drawing.Size(57, 17);
            this.RBc.TabIndex = 1;
            this.RBc.TabStop = true;
            this.RBc.Text = "Circulo";
            this.RBc.UseVisualStyleBackColor = true;
            this.RBc.CheckedChanged += new System.EventHandler(this.RBc_CheckedChanged);
            // 
            // RBr
            // 
            this.RBr.AutoSize = true;
            this.RBr.Location = new System.Drawing.Point(41, 28);
            this.RBr.Name = "RBr";
            this.RBr.Size = new System.Drawing.Size(80, 17);
            this.RBr.TabIndex = 0;
            this.RBr.TabStop = true;
            this.RBr.Text = "Rectangulo";
            this.RBr.UseVisualStyleBackColor = true;
            this.RBr.CheckedChanged += new System.EventHandler(this.RBr_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtH);
            this.groupBox2.Controls.Add(this.txtR);
            this.groupBox2.Controls.Add(this.txtA);
            this.groupBox2.Controls.Add(this.txtL);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 148);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos de la figura";
            // 
            // txtH
            // 
            this.txtH.Location = new System.Drawing.Point(77, 118);
            this.txtH.Name = "txtH";
            this.txtH.Size = new System.Drawing.Size(100, 20);
            this.txtH.TabIndex = 7;
            // 
            // txtR
            // 
            this.txtR.Location = new System.Drawing.Point(77, 86);
            this.txtR.Name = "txtR";
            this.txtR.Size = new System.Drawing.Size(100, 20);
            this.txtR.TabIndex = 6;
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(77, 60);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(100, 20);
            this.txtA.TabIndex = 5;
            // 
            // txtL
            // 
            this.txtL.Location = new System.Drawing.Point(77, 32);
            this.txtL.Name = "txtL";
            this.txtL.Size = new System.Drawing.Size(100, 20);
            this.txtL.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Altura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Radio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ancho";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Largo";
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(268, 90);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(96, 34);
            this.btn1.TabIndex = 2;
            this.btn1.Text = "Calcular Area";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(268, 196);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(96, 34);
            this.btn2.TabIndex = 3;
            this.btn2.Text = "Salir";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 345);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RBt;
        private System.Windows.Forms.RadioButton RBc;
        private System.Windows.Forms.RadioButton RBr;
        private System.Windows.Forms.TextBox txtH;
        private System.Windows.Forms.TextBox txtR;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.TextBox txtL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;

    }
}

