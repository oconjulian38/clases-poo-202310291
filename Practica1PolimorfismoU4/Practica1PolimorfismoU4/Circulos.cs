﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1PolimorfismoU4
{
    class Circulos: Figuras
    {
        public double Radio;

        public override void CalcularFiguras(double n1, double n2)
        {
            base.CalcularFiguras(n1, n2);
            Radio = n1 * n1 * 3.14;
        }
    }
}
