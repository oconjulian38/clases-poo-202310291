﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica1SalariosHerencia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn1Calcular_Click(object sender, EventArgs e)
        {
            if (RBIntendentes.Checked == true)
            {
                Intendentes objI = new Intendentes();
                double horastrabaI = double.Parse(txt1hortra.Text);
                double horasextI = double.Parse(txthorext.Text);
                objI.CalcularSalarios(horastrabaI, horasextI);
                MessageBox.Show("El pago por las horas extras trabajadas es: " + objI.PagoExtra, "El pago por las horas trabajadas es: " + objI.PagoNormal);
            }

            if (RBProduccion.Checked == true)
            {
                Produccion objP = new Produccion();
                double horastrabaP = double.Parse(txt1hortra.Text);
                double horasextP = double.Parse(txthorext.Text);
                objP.CalcularSalarios(horastrabaP, horasextP);
                MessageBox.Show("El pago por las horas extras trabajadas es: " + objP.Pagoextras, "El pago por las horas trabajadas es: " + objP.Pagonormal);
            }

            if (RBAdministrativos.Checked == true)
            {
                Administrativos objA = new Administrativos();
                double horastrabaA = double.Parse(txt1hortra.Text);
                double horasextA = double.Parse(txthorext.Text);
                objA.CalcularSalarios(horastrabaA, horasextA);
                MessageBox.Show("El pago por las horas estras trabajadas es: " + objA.Pagoext, "El pago por las horas trabajadas es: " + objA.Pagonormal);
            }

            if (RBGerencias.Checked == true)
            {
                Gerencias objG = new Gerencias();
                double horastrabaG = double.Parse(txt1hortra.Text);
                double horasextG = double.Parse(txthorext.Text);
                objG.CalcularSalarios(horastrabaG, horasextG);
                MessageBox.Show("El pago por las horas estras trabajadas es: " + objG.Pagoextras, "El pago por las horas trabajadas es: " + objG.Pagonormal);
            }

            if (RBDueños.Checked == true)
            {
                Dueños objD = new Dueños();
                double horastrabaD = double.Parse(txt1hortra.Text);
                double horasextD = double.Parse(txthorext.Text);
                objD.CalcularSalarios(horastrabaD, horasextD);
                MessageBox.Show("El pago por las horas estras trabajadas es: " + objD.Pagoextras, "El pago por las horas trabajadas es: " + objD.Pagonormal);
            }

        }

        private void btn2Salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
