﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1SalariosHerencia
{
    class Administrativos: Salarios
    {
        public double Pagonormal, Pagoext;

        public override void CalcularSalarios(double S1, double S2)
        {
            base.CalcularSalarios(S1, S2);
            Pagonormal = S1 * 50.00;
            Pagoext = S2 * 50.00;
        }
    }
}
