﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1SalariosHerencia
{
    class Dueños: Salarios
    {
        public double Pagonormal, Pagoextras;

        public override void CalcularSalarios(double S1, double S2)
        {
            base.CalcularSalarios(S1, S2);
            Pagonormal = S1 * 200.00;
            Pagoextras = S2 * 200.00;
        }
    }
}
