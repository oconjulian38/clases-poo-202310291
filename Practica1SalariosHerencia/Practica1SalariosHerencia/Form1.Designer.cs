﻿namespace Practica1SalariosHerencia
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RBIntendentes = new System.Windows.Forms.RadioButton();
            this.RBProduccion = new System.Windows.Forms.RadioButton();
            this.RBAdministrativos = new System.Windows.Forms.RadioButton();
            this.RBGerencias = new System.Windows.Forms.RadioButton();
            this.RBDueños = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt1hortra = new System.Windows.Forms.TextBox();
            this.txthorext = new System.Windows.Forms.TextBox();
            this.btn1Calcular = new System.Windows.Forms.Button();
            this.btn2Salir = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // RBIntendentes
            // 
            this.RBIntendentes.AutoSize = true;
            this.RBIntendentes.Location = new System.Drawing.Point(6, 19);
            this.RBIntendentes.Name = "RBIntendentes";
            this.RBIntendentes.Size = new System.Drawing.Size(81, 17);
            this.RBIntendentes.TabIndex = 0;
            this.RBIntendentes.TabStop = true;
            this.RBIntendentes.Text = "Intendentes";
            this.RBIntendentes.UseVisualStyleBackColor = true;
            // 
            // RBProduccion
            // 
            this.RBProduccion.AutoSize = true;
            this.RBProduccion.Location = new System.Drawing.Point(6, 53);
            this.RBProduccion.Name = "RBProduccion";
            this.RBProduccion.Size = new System.Drawing.Size(79, 17);
            this.RBProduccion.TabIndex = 1;
            this.RBProduccion.TabStop = true;
            this.RBProduccion.Text = "Produccion";
            this.RBProduccion.UseVisualStyleBackColor = true;
            // 
            // RBAdministrativos
            // 
            this.RBAdministrativos.AutoSize = true;
            this.RBAdministrativos.Location = new System.Drawing.Point(6, 85);
            this.RBAdministrativos.Name = "RBAdministrativos";
            this.RBAdministrativos.Size = new System.Drawing.Size(95, 17);
            this.RBAdministrativos.TabIndex = 2;
            this.RBAdministrativos.TabStop = true;
            this.RBAdministrativos.Text = "Administrativos";
            this.RBAdministrativos.UseVisualStyleBackColor = true;
            // 
            // RBGerencias
            // 
            this.RBGerencias.AutoSize = true;
            this.RBGerencias.Location = new System.Drawing.Point(6, 119);
            this.RBGerencias.Name = "RBGerencias";
            this.RBGerencias.Size = new System.Drawing.Size(73, 17);
            this.RBGerencias.TabIndex = 3;
            this.RBGerencias.TabStop = true;
            this.RBGerencias.Text = "Gerencias";
            this.RBGerencias.UseVisualStyleBackColor = true;
            // 
            // RBDueños
            // 
            this.RBDueños.AutoSize = true;
            this.RBDueños.Location = new System.Drawing.Point(6, 152);
            this.RBDueños.Name = "RBDueños";
            this.RBDueños.Size = new System.Drawing.Size(62, 17);
            this.RBDueños.TabIndex = 4;
            this.RBDueños.TabStop = true;
            this.RBDueños.Text = "Dueños";
            this.RBDueños.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RBIntendentes);
            this.groupBox1.Controls.Add(this.RBDueños);
            this.groupBox1.Controls.Add(this.RBProduccion);
            this.groupBox1.Controls.Add(this.RBGerencias);
            this.groupBox1.Controls.Add(this.RBAdministrativos);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 175);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trabajadores";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txthorext);
            this.groupBox2.Controls.Add(this.txt1hortra);
            this.groupBox2.Location = new System.Drawing.Point(199, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(216, 170);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Horas de Trabajo";
            // 
            // txt1hortra
            // 
            this.txt1hortra.Location = new System.Drawing.Point(110, 48);
            this.txt1hortra.Name = "txt1hortra";
            this.txt1hortra.Size = new System.Drawing.Size(100, 20);
            this.txt1hortra.TabIndex = 0;
            // 
            // txthorext
            // 
            this.txthorext.Location = new System.Drawing.Point(110, 98);
            this.txthorext.Name = "txthorext";
            this.txthorext.Size = new System.Drawing.Size(100, 20);
            this.txthorext.TabIndex = 1;
            // 
            // btn1Calcular
            // 
            this.btn1Calcular.Location = new System.Drawing.Point(86, 207);
            this.btn1Calcular.Name = "btn1Calcular";
            this.btn1Calcular.Size = new System.Drawing.Size(107, 33);
            this.btn1Calcular.TabIndex = 7;
            this.btn1Calcular.Text = "Calcular Salarios";
            this.btn1Calcular.UseVisualStyleBackColor = true;
            this.btn1Calcular.Click += new System.EventHandler(this.btn1Calcular_Click);
            // 
            // btn2Salir
            // 
            this.btn2Salir.Location = new System.Drawing.Point(238, 207);
            this.btn2Salir.Name = "btn2Salir";
            this.btn2Salir.Size = new System.Drawing.Size(111, 33);
            this.btn2Salir.TabIndex = 8;
            this.btn2Salir.Text = "Salir";
            this.btn2Salir.UseVisualStyleBackColor = true;
            this.btn2Salir.Click += new System.EventHandler(this.btn2Salir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Horas trabajadas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Horas Extras";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 261);
            this.Controls.Add(this.btn2Salir);
            this.Controls.Add(this.btn1Calcular);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Salarios";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton RBIntendentes;
        private System.Windows.Forms.RadioButton RBProduccion;
        private System.Windows.Forms.RadioButton RBAdministrativos;
        private System.Windows.Forms.RadioButton RBGerencias;
        private System.Windows.Forms.RadioButton RBDueños;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txthorext;
        private System.Windows.Forms.TextBox txt1hortra;
        private System.Windows.Forms.Button btn1Calcular;
        private System.Windows.Forms.Button btn2Salir;
    }
}

