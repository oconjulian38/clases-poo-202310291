﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1SalariosHerencia
{
    class Intendentes: Salarios
    {
        public double PagoNormal, PagoExtra;

        public override void CalcularSalarios(double S1, double S2)
        {
            base.CalcularSalarios(S1, S2);
            PagoNormal = S1 * 20.00;
            PagoExtra = S2 * 20.00;
        }
    }
}
