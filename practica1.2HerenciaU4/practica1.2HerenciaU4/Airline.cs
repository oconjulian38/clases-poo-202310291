﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica1._2HerenciaU4
{
    class Airline
    {
        public string reserva;
        public string destinos;
        public string salidas;
        public string FechaDeVue;
        public string TiempodeVue;
        public int pre;
        public int numerodevue;

        public void Disponibilidad_de_Reserva()
        {
            Console.WriteLine("¿Quiere hacer una reserva? si/no");
            reserva = Convert.ToString(Console.ReadLine());

            if (reserva == "si")
            {
                Flight obj = new Flight();
                obj.Destino();
                obj.Salida();
                obj.FechaDeVuelo();
                obj.TiempoDeVuelo();
                obj.PrecioDeVuelo();
                obj.NumeroDeVuelo();
            }
            else
            {
                Console.WriteLine("Pues no");
            }   
        }
    }
    class Flight : Airline
    {
        public void Destino()
        {
            Console.WriteLine("Ingresa el destino de tu vuelo: ");
            destinos = Convert.ToString(Console.ReadLine());
        }

        public void Salida()
        {
            Console.WriteLine("Ingresa la fecha de salida: ");
            salidas = Convert.ToString(Console.ReadLine());
        }

        public void FechaDeVuelo()
        {
            Console.WriteLine("Ingresa la fecha del vuelo");
            FechaDeVue = Convert.ToString(Console.ReadLine());
        }

        public void TiempoDeVuelo()
        {
            Console.WriteLine("Ingresa el tiempo del vuelo");
            TiempodeVue = Convert.ToString(Console.ReadLine());
        }

        public void PrecioDeVuelo()
        {
            Console.WriteLine("Ingresa el precio: ");
            pre = Convert.ToInt32(Console.ReadLine());
        }

        public void NumeroDeVuelo()
        {
            Console.WriteLine("Ingresa el numero del vuelo: ");
            numerodevue = Convert.ToInt32(Console.ReadLine());
        }
    }
}
