﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica3SobrecargadeMetodosAlCuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[2];
            double[] b = new double[2];

            Elevar obj = new Elevar();

            Console.WriteLine("Numeros a elevar a:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }


            Console.WriteLine("Numeros a elevar b:");
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = Convert.ToDouble(Console.ReadLine());
            }

            obj.Calcular(a);
            obj.Calcular(b);

            Console.ReadLine();
        }
    }
}
