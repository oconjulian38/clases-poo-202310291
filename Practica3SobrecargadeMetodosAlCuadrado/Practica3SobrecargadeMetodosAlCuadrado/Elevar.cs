﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica3SobrecargadeMetodosAlCuadrado
{
    class Elevar
    {
        
        public void Calcular(params int[] a)
        {
            Console.WriteLine("Numeros elevados a");
            
            foreach (int i in a)
            {
                Console.WriteLine(Math.Pow(+i,2));
            }
        }

        public void Calcular(params double[] b)
        {
            Console.WriteLine("Numeros elevados b");

            foreach (double i in b)
            {
                Console.WriteLine(Math.Pow(+i,2));
            }
        }

    }
}
