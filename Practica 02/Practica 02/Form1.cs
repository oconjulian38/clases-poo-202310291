﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();
            TV1.setTamanio(30);
            int tamanioTV1 = TV1.getTamanio();

            TV1.setVolumen(10);
            int volumenTV1 = TV1.getVolumen();

            TV1.setColor("negro");
            string colorTV1 = TV1.getColor();

            TV1.setContraste(7);
            int contrasteTV1 = TV1.getContraste();

            TV1.setMarca("Sony");
            string marcaTV1 = TV1.getMarca();

            TV1.setBrillo(6);
            int brilloTV1 = TV1.getBrillo();

            MessageBox.Show("El tamaño de la tv es: " + tamanioTV1 + " Pulgadas");
            MessageBox.Show("El volumen de la tv es: " + volumenTV1);
            MessageBox.Show("El color de la tv es: " + colorTV1);
            MessageBox.Show("El contraste de la tv es: " + contrasteTV1);
            MessageBox.Show("La marca de la tv es: " + marcaTV1);
            MessageBox.Show("El brillo de la tv es: " + brilloTV1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
