﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoU4Sobrecarga
{
    class Program
    {
        static void Main(string[] args)
        {
            SobrecargadeMetodos Carga = new SobrecargadeMetodos();
            Carga.Calcular(4,7, 9, 5);
            Carga.Calcular(5, 9, 3);
            Console.ReadKey();
        }
    }
}
