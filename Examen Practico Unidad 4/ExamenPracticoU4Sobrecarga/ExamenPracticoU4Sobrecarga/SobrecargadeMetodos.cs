﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoU4Sobrecarga
{
    class SobrecargadeMetodos
    {
        public void Calcular(double x, double y, double z, double a)
        {
            double resultado = x + y + z + a;
            double total = resultado / 4;
            Console.WriteLine("El resultado es: " + total);
        }

        public void Calcular(double x, double y, double z)
        {
            double resultado = x / y * z;
            double total = resultado * 3;
            Console.WriteLine("El resultado es: " + total);
        }
    }
}
