﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosAbstractasExamenPracticoU4
{
    class Hijo: ClaseAbstracta
    {
        public override void Calcular(double num1, double num2)
        {
            double resultado = num2 * num1 / num2;
            Console.WriteLine("El total es: " + resultado);
        }
    }
}
