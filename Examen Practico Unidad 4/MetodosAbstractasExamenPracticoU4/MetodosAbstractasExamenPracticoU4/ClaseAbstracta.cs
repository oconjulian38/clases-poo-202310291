﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodosAbstractasExamenPracticoU4
{
    abstract class ClaseAbstracta
    {
        abstract void Calcular(double num1, double num2)
        {
            double resultado = num1 / num2 * num2;
            Console.WriteLine("El resultado de la division de la clase abstracta es: " + resultado);
        }
    }
}
