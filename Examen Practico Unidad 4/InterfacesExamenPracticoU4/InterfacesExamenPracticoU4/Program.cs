﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesExamenPracticoU4
{
    class Program
    {
        static void Main(string[] args)
        {
            ClaseHeredada obj = new ClaseHeredada();
            decimal numero = 15.15M;
            Console.WriteLine(obj.Calcular(numero).ToString());
            int num1 = 182;
            Console.WriteLine(obj.Operar(num1).ToString());
            Console.ReadKey();
        }
    }
}
