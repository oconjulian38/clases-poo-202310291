﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPractico_SegundoParcial
{
    class Operacion
    {
        public double total, monto_venta, bono_final, respuesta;
        private double contador;
        private double bono1, bono2, bono3;

        public Operacion()
        {
            total = 0;
            contador = 0;
            bono1 = 0.07;
            bono2 = 0.12;
            bono3 = 0.17;
            
        }


        
        public void operacion()
        {
            do
            {
                contador++;
                Console.WriteLine("Ingresa el monto de venta de este mes: ");
                monto_venta = double.Parse(Console.ReadLine());
                


                if ((monto_venta > 0) && (monto_venta <= 250))
                {
                    Console.WriteLine("El bono adquirido es del 7%");
                    bono_final = monto_venta * bono1;
                    total = monto_venta + bono_final;
                    Console.WriteLine("El total es: {0}", total);
                }
                if ((monto_venta > 250) && (monto_venta <= 750))
                {
                    Console.WriteLine("el bono adquirido es del 12%");
                    bono_final = monto_venta * bono2;
                    total = monto_venta + bono_final;
                    Console.WriteLine("El total es: {0}", total);
                }
                if ((monto_venta > 750) && (monto_venta <= 1000))
                {
                    Console.WriteLine("el bono adquirido es del 17%");
                    bono_final = monto_venta * bono3;
                    total = monto_venta + bono_final;
                    Console.WriteLine("El total es: {0}", total);
                }
                Console.WriteLine("¿Quieres ingresar mas montos? Cualquier numero=SI, 0=NO");
                respuesta = double.Parse(Console.ReadLine());

            } while (respuesta != 0);
            

        }
    
    }
}
